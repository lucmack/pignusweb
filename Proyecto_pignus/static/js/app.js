var dateObj = new Date();
var month = dateObj.getUTCMonth() + 1; //months from 1-12
var day = dateObj.getUTCDate();
var year = dateObj.getUTCFullYear();
var today = `${year}-0${month}-${day}`
console.log(today)
newdate = year + "/" + month + "/" + day;
$("#formulario").validate({
    errorClass:"is-invalid",
    rules:{
        nombre:{
            required:true
        },
        apellido:{
            required:true
        },
        email:{
            required:true,
            email:true
        },
        fnacimiento:{
            max:today
        },
        password:{
            required:true
        }
    },
    messages:{
        nombre:{
            required:"su nombre es requerido"
        },
        apellido:{
            required:"su apellido es requerido"
        },
        email:{
            required:"Su correo es requerido ",
            email:"se requiere un email valido "
        },
        password:{
            required:"Se requiere ingresar una contraseña ",
           
        }
    }
})