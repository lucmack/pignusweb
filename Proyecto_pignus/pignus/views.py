from django.shortcuts import render

# Create your views here.
def index(request):
    return render(request,'index.html',{})

def contactenos(request):
    contex = {'titulo':'Contactenos','verdadero':False}
    return render(request,'contactenos.html',contex)